﻿using System.Collections;
using UnityEngine;

public class Attack : MonoBehaviour
{
    private enum CharacterType { hero, enemy}
    [SerializeField] private CharacterType characterType;

    public SpriteRenderer[] shootingFXs;

    private GameObject currentBullet;
    private LevelManager manager;

    private void Awake()
    {
        SetShootingFXsEnabled(false);
    }

    private void Start()
    {
        manager = LevelManager.Instance;
        if (characterType == CharacterType.enemy) StartCoroutine(AutoAttacking());
    }

    public void HeroAttack()
    {
        if (currentBullet == null || currentBullet.activeSelf == false)
            foreach (SpriteRenderer fx in shootingFXs) Fire(fx, 0.2f);
    }

    private void Fire(SpriteRenderer shootingFX, float duration)
    {
        GameObject obj = null;
        switch (characterType)
        {
            case CharacterType.hero:
                obj = manager.PullFromPool(manager.laserPool);
                break;
            case CharacterType.enemy:
                obj = manager.PullFromPool(manager.rocketsPool);
                break;
            default:
                break;
        }

        if (obj != null)
        {
            currentBullet = obj;

            obj.transform.position = shootingFX.GetComponentInParent<Transform>().position;
            
            StopCoroutine(FireBlitz(duration));
            StartCoroutine(FireBlitz(duration));
        }
    }

    private void SetShootingFXsEnabled(bool _enabled)
    {
        foreach (SpriteRenderer fx in shootingFXs) fx.enabled = _enabled;
    }

    private IEnumerator AutoAttacking()
    {
        while (true)
        {
            if (currentBullet == null || currentBullet.activeSelf == false)
            {
                if (manager.CurrentEnemiesShooting < manager.CurrentEnemiesShootingLimit)
                {
                    foreach (SpriteRenderer shootingFX in shootingFXs)
                    {
                        RaycastHit2D hit = Physics2D.Raycast(origin: shootingFX.gameObject.transform.position, direction: Vector3.down, Mathf.Infinity);
                        if (hit.collider != null && hit.collider.gameObject.tag == "Player")
                        {
                            manager.CurrentEnemiesShooting++;
                            Fire(shootingFX, 0.2f);
                        }
                    }
                }
            }

            yield return null;
        }
    }

    private IEnumerator FireBlitz(float seconds)
    {
        bool status = true;
        SetShootingFXsEnabled(status);
        while (seconds > 0)
        {
            status = !status;
            SetShootingFXsEnabled(status);
            yield return null;
            seconds -= Time.deltaTime;
        }
        SetShootingFXsEnabled(false);
    }
}
