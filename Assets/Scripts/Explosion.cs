﻿using UnityEngine;

public class Explosion : MonoBehaviour
{
    public void DisableExplosion()
    {
        Invoke("DisableMe", 0.2f);
    }

    private void DisableMe()
    {
        LevelManager.Instance.PushToPool(LevelManager.Instance.explosionsPool, gameObject);
    }
}
