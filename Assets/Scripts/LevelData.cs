﻿using UnityEngine;


[System.Serializable]
[CreateAssetMenu]
public class LevelData : ScriptableObject
{
    public int enemiesInRow;
    public int numberOfRows;
    public float stepBetween;

    public float movementSpeed;
    public float speedIncreaseValue;

    public int maxEnemiesShooting;
}
